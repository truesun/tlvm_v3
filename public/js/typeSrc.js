$(function () {
    //typeing
    labelToSrc('#tagCheckList', '#tagSrc', '#tagSrcBtn', '#tagSrcCleanBtn');
    labelToSrc('#eventCheckList', '#eventCheckSrc', '#eventCheckBtn', '#eventCheckCleanBtn');
    labelToSrc('#moreBookList', '#moreSrc', '#moreSrcBtn', '#moreSrcCleanBtn');
    //lightbox 標籤選擇
    labelToSrc('#tagEnCheckList', '#tagSrcEn', '#tagSrcEnBtn', '#tagSrcCleanEnBtn');

    labelToSrc('#authorList', '#authorSrc', '#authorSrcBtn', '#authorSrcCleanBtn');
    labelToSrc('#authorEnList', '#authorSrcEn', '#authorSrcEnBtn', '#authorSrcCleanEnBtn');

    labelToSrc('#moreBookEnList', '#moreSrcEn', '#moreSrcEnBtn', '#moreSrcCleanEnBtn');

    labelToSrc('#eventCheckEnList', '#eventCheckSrcEn', '#eventCheckEnBtn', '#eventCheckCleanEnBtn');

    labelToSrc('#routeCheckList', '#routeCheckSrc', '#routeCheckSrcBtn', '#routeCheckCleanBtn');
    labelToSrc('#routeCheckEnList', '#routeCheckSrcEn', '#routeCheckSrcEnBtn', '#routeCheckCleanEnBtn');

    labelToSrc('#newsCheckList', '#newsCheckSrc', '#newsCheckSrcBtn', '#newsCheckCleanBtn');
    labelToSrc('#newsCheckEnList', '#newsCheckSrcEn', '#newsCheckSrcEnBtn', '#newsCheckCleanEnBtn');

    labelToSrc('#eventRouteCheckList', '#eventRouteCheckSrc', '#eventRouteCheckSrcBtn', '#eventRouteCheckCleanBtn');
    labelToSrc('#eventRouteCheckEnList', '#eventRouteCheckSrcEn', '#eventRouteCheckSrcEnBtn', '#eventRouteCheckCleanEnBtn');

    labelToSrc('#sendGroupCheckList', '#sendGroupCheckSrc', '#sendGroupCheckSrcBtn', '#sendGroupCheckCleanBtn');

    labelToSrc('#worksheetCheckList', '#worksheetCheckSrc', '#worksheetCheckSrcBtn', '#worksheetCheckCleanBtn');
    labelToSrc('#worksheetCheckEnList', '#worksheetCheckSrcEn', '#worksheetCheckSrcEnBtn', '#worksheetCheckCleanEnBtn');
    //= 學習單 =
    labelToSrc('#worksheetsCheckList', '#worksheetsSrc', '#worksheetsSrcBtn', '#worksheetsSrcCleanBtn');
    labelToSrc('#worksheetsEnCheckList', '#worksheetsSrcEn', '#worksheetsSrcEnBtn', '#worksheetsSrcCleanEnBtn');
    //= 引用路線 =
    labelToSrc('#routeTagCheckList', '#routeTagSrc', '#routeTagSrcBtn', '#routeTagSrcCleanBtn');
    labelToSrc('#routeTagCheckEnList', '#routeTagSrcEn', '#routeTagSrcEnBtn', '#routeTagSrcCleanEnBtn');
});
//labelToSrc(最外層Box,輸入匡ID,收尋按鈕,清除按鈕)
function labelToSrc(listBox, srcInput, srcBtn, cleanBtn) {
    var $listBox = $(listBox);
    var $srcInput = $(srcInput);
    var $srcBtn = $(srcBtn);
    var $cleanBtn = $(cleanBtn);
    //搜尋放入資料
    labelSrc(listBox, srcInput);
    //搜尋
    $srcBtn.on('click', function () {
        srcLabel(listBox, srcInput);
    });
    //清除搜尋
    $cleanBtn.on('click', function () {
        $srcInput.val('');
        $listBox.find('label').show();
        $listBox.find('.no-data').hide();
    });
}
//搜尋
function srcLabel(listBox, srcInput) {
    var $listBox = $(listBox);
    var $srcInput = $(srcInput);

    var keyWord = $srcInput.val();
    $listBox.find('span').each(function (index, item) {
        var txt = $(this).text();
        if (txt.indexOf(keyWord) == -1) {
            $(this).parent().hide();
        }
    });
    var tagNum = $listBox.find('span:visible').length;
    tagNum ? $listBox.find('.no-data').hide() : $listBox.find('.no-data').show();
}
//收尋放入資料
function labelSrc(listBox, srcInput) {
    var $listBox = $(listBox);
    $listBox.append('<div class="no-data" style="display: none;">無此關鍵字</div>');
    var $srcInput = $(srcInput);
    //搜尋資料陣列
    var srcList = [];

    $listBox.find('span').each(function (index, item) {
        var txt = $(this).text();
        srcList.push(txt);
    });

    var substringMatcher = function (strs) {
        return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function (i, str) {
                if (substrRegex.test(str)) {
                    matches.push(str);
                }
            });

            cb(matches);
        };
    };

    $srcInput.typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    },
        {
            name: 'srcList',
            limit: 99,//如果沒有設定，最多只會出現五筆
            source: substringMatcher(srcList)
        });
}