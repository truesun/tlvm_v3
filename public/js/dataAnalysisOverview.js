$(function () {
    //=== 取得API ===
    //== 每日人流 ==
    var apiLink = '/overview.aspx?Type=3'
    var dataList;
    //= 取得搜尋日期 =
    var dataDateList = [];
    //= 取得攝影機列表 =
    var cameraNameList = [];
    $.get(apiLink, function (data) {
        // console.log(data);
        dataList = JSON.parse(data);
        console.log('=== 每日人流 ===');
        console.log(dataList);
        $.each(dataList, function (ind, val) {
            var { timestamp, CameraName, Acount } = val;
            //放入搜尋時間
            var hasInd = dataDateList.findIndex(function (item) {
                return item == timestamp;
            });
            if (hasInd == -1) {
                dataDateList.push(timestamp);
            }
            //放入攝影機名稱
            var hasCamInd = cameraNameList.findIndex(function (item) {
                return item == CameraName;
            });
            if (hasCamInd == -1) {
                cameraNameList.push(CameraName);
            }
        });
        // var camera1 = dataList.filter(function (item) {
        //     return item.CameraName == '台文館正門';// { 'uid': '001', 'name': 'simple' }
        // });
        // console.log(camera1);
    });
    var dataDate = [];
    $.each(dataDateList, function (ind, val) {
        var timestampS = val.split('-');
        var timestampSDay = timestampS[1] + '/' + timestampS[2];
        dataDate.push(timestampSDay);
    });
    //= 圖表資料 =
    var chartData = [];
    $.each(cameraNameList, function (ind, valName) {
        var list = dataList.filter(function (item) {
            return item.CameraName == valName;
        });
        //= 總計點點 =
        var dataDot = [];
        $.each(list, function (ind, val) {
            var { Acount } = val;
            //== data ==
            dataDot.push(Number(Acount));
        });
        chartData.push({ 'name': valName, 'data': dataDot });
    });
    //== 放入圖表 ==
    lineChart(dataDate, chartData);

    //== 前一週日期 ==
    var yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    var yesterdayY = yesterday.getFullYear();
    var yesterdayM = yesterday.getMonth() + 1;
    var preWeek = new Date();
    preWeek.setDate(preWeek.getDate() - 7);
    var preWeekY = preWeek.getFullYear();
    var preWeekM = preWeek.getMonth() + 1;
    var preWeekTxt = preWeekY + '/' + preWeekM + '/' + preWeek.getDate() + ' - ' + yesterdayY + '/' + yesterdayM + '/' + yesterday.getDate();
    //== 客群性別 ==
    var apiLink = '/overview.aspx?Type=4';
    var pieData = [];
    $.get(apiLink, function (data) {
        // console.log(data);
        var dataList = JSON.parse(data);
        console.log('=== 客群性別 ===');
        console.log(dataList);
        var { F, M } = dataList[0];
        var MNum = Number(M).toFixed(2);
        var FNum = Number(F).toFixed(2);
        pieData.push({ name: "男性", y: Number(MNum) }, { name: "女性", y: Number(FNum) });
    });
    var pieColor = ['#88cff9', '#f98888'];
    pieChart('container1', pieColor, '客群性別分佈佔比', preWeekTxt, pieData);

    //== 客群年紀 ==
    var apiLink = '/overview.aspx?Type=5';
    var pieData2 = [];
    $.get(apiLink, function (data) {
        // console.log(data);
        var dataList = JSON.parse(data);
        console.log('=== 客群年記 ===');
        console.log(dataList);
        $.each(dataList, function (ind, val) {
            var yA = Number(val.A).toFixed(2);
            pieData2.push({ name: '(0-17歲)', y: Number(yA) });
            var yB = Number(val.B).toFixed(2);
            pieData2.push({ name: '(18-24歲)', y: Number(yB) });
            var yC = Number(val.C).toFixed(2);
            pieData2.push({ name: '(25-34歲)', y: Number(yC) });
            var yD = Number(val.D).toFixed(2);
            pieData2.push({ name: '(35-44歲)', y: Number(yD) });
            var yE = Number(val.E).toFixed(2);
            pieData2.push({ name: '(45-54歲)', y: Number(yE) });
            var yF = Number(val.F).toFixed(2);
            pieData2.push({ name: '(55-64歲)', y: Number(yF) });
            var yG = Number(val.G).toFixed(2);
            pieData2.push({ name: '(65+歲)', y: Number(yG) });
        });
    });
    var pieColor2 = ['#FFAD88', '#B978FA', '#F88D8D', '#FFB3B3', '#9ABCEF', '#F3E15D', '#B2C15A'];
    pieChart('container2', pieColor2, '客群年齡分佈佔比', preWeekTxt, pieData2);

    // var data={
    //     'dateList': ['2021-06-01', '2021-06-02'],//搜尋單位
    //     'point':[
    //         {
    //             'name':'台文館正門',
    //             'data':[ 10, 20 ]//以搜尋單位排列 2021-06-01 的數量  2021-06-02的數量...
    //         },
    //         {
    //             'name': '台文館咖啡廳',
    //             'data': [ 11, 22]
    //         }
    //     ]
    // }
    // var data = [
    //     {
    //         'date': '2021-01-01 20:00',
    //         'point': [
    //             {
    //                 'CameraName': 'Total',
    //                 'Acount': 30,
    //             },
    //             {
    //                 'CameraName': '台文館正門',
    //                 'Acount': 10,
    //             },
    //             {
    //                 'CameraName': '台文館咖啡廳',
    //                 'Acount': 20,
    //             },
    //         ]
    //     },
    //     {
    //         'date': '2021-01-01 21:00',
    //         'point': [
    //             {
    //                 'CameraName': 'Total',
    //                 'Acount': 10,
    //             },
    //             {
    //                 'CameraName': '台文館正門',
    //                 'Acount': 5,
    //             },
    //             {
    //                 'CameraName': '台文館咖啡廳',
    //                 'Acount': 5,
    //             },
    //         ]
    //     }
    // ]
    // var dateList = [];
    // var pointList = [
    //     {
    //         name: 'Total',
    //         data: [],
    //     },
    //     {
    //         name: '台文館正門',
    //         data: [],
    //     },
    //     {
    //         name: '台文館咖啡廳',
    //         data: [],
    //     },
    // ];
    // $.each(data, function (ind, val) {
    //     var { date, point } = val;
    //     dateList.push(date);
    //     $.each(point, function (ind, val) {
    //         var { CameraName, Acount } = val;
    //         var cameraInd = pointList.findIndex(function (item) {
    //             return item.name == CameraName;
    //         });
    //         pointList[cameraInd].data.push(Acount);
    //     });
    // });
    // console.log(dateList);
    // console.log(pointList);

    // var test = [
    //     {
    //         timestamp: "2021-05-17",
    //         CameraName: "Total",
    //         Acount: "0"
    //     },
    //     {
    //         timestamp: "2021-05-17",
    //         CameraName: "台文館正門",
    //         Acount: "0"
    //     },
    //     {
    //         timestamp: "2021-05-17",
    //         CameraName: "台文館咖啡廳",
    //         Acount: "0"
    //     }
    // ]
    // var ind = test.findIndex(function (item) {
    //     return item.timestamp == '2021-05-17' && item.CameraName == '台文館咖啡廳';
    // });
    // console.log('=== 我是第幾個 ===');
    // console.log(ind);
});
//=== 影像折線圖 ===
function lineChart(yData, chartData) {
    // A point click event that uses the Renderer to draw a label next to the point
    // On subsequent clicks, move the existing label instead of creating a new one.
    Highcharts.addEvent(Highcharts.Point, 'click', function () {
        if (this.series.options.className.indexOf('popup-on-click') !== -1) {
            const chart = this.series.chart;
            // const date = Highcharts.dateFormat('%A, %b %e, %Y', this.x);
            // const text = `<b>${date}</b><br />${this.y} ${this.series.name}`;
            const text = `<b>${this.category}</b><br />${this.series.name} : ${this.y} 人`;

            const anchorX = this.plotX + this.series.xAxis.pos;
            const anchorY = this.plotY + this.series.yAxis.pos;
            const align = anchorX < chart.chartWidth - 200 ? 'left' : 'right';
            const x = align === 'left' ? anchorX + 10 : anchorX - 10;
            const y = anchorY - 30;
            if (!chart.sticky) {
                chart.sticky = chart.renderer
                    .label(text, x, y, 'callout', anchorX, anchorY)
                    .attr({
                        align,
                        fill: 'rgba(0, 0, 0, 0.75)',
                        padding: 10,
                        zIndex: 7 // Above series, below tooltip
                    })
                    .css({
                        color: 'white'
                    })
                    .on('click', function () {
                        chart.sticky = chart.sticky.destroy();
                    })
                    .add();
            } else {
                chart.sticky
                    .attr({ align, text })
                    .animate({ anchorX, anchorY, x, y }, { duration: 250 });
            }
        }
    });

    //chart color
    Highcharts.setOptions({
        colors: ['#7cb4ec', '#f7ce6b', '#91ed7d', '#f7a45b', '#8085e8', '#f25d80'],
        // colors: ['#ed6e85', '#f1a354', '#f7ce6b', '#6dbebf', '#57a1e5', '#de82df'],
        // #7cb4ec #000 #91ed7d #f7a45b #8085e8 #f25d80
        //資料千分位
        lang: {
            thousandsSep: ','
        }
    });

    Highcharts.chart('container', {

        chart: {
            scrollablePlotArea: {
                minWidth: 700
            }
        },

        //   data: {
        //     csvURL: '../data/analytics.csv',
        //     beforeParse: function (csv) {
        //       return csv.replace(/\n\n/g, '\n');
        //     }
        //   },

        title: {
            text: '每日人流數據分析統計'
        },

        subtitle: {
            text: '不包含當日人數'
        },

        xAxis: {
            categories: yData
            // categories: ['4/22', '4/23', '4/24', '4/25', '4/26', '4/27', '4/28', '4/29', '4/30', '5/1', '5/2', '5/3', '5/4', '5/5', '5/6', '5/7', '5/8', '5/9', '5/10', '5/11', '5/12', '5/13', '5/14', '5/15', '5/16', '5/17', '5/18', '5/19', '5/20', '5/21']
        },

        yAxis: [{ // left y axis
            title: {
                text: null
            },
            labels: {
                align: 'left',
                x: 3,
                y: 16,
                format: '{value:.,0f}'
            },
            showFirstLabel: false
        },
        { // right y axis
            linkedTo: 0,
            gridLineWidth: 0,
            opposite: true,
            title: {
                text: null
            },
            labels: {
                align: 'right',
                x: -3,
                y: 16,
                format: '{value:.,0f}'
            },
            showFirstLabel: false
        }],

        legend: {
            // layout: 'vertical',//水平對齊
            align: 'right',// 圖表點點資料左右
            verticalAlign: 'top',
            // align: 'left',
            // verticalAlign: 'top',
            // borderWidth: 0
        },
        //顯示資料table
        tooltip: {
            shared: true,//是否可以同時觀看
            crosshairs: true,//Ｙ軸輔助
            // xDateFormat: '%Y-%m-%d',//資料日期格式 
            // valueDecimals: 2,//資料小數點幾位
            // valuePrefix: '$',//資料前字串
            valueSuffix: '<span style="font-size: 8px">人</span>',//資料後字串
            // headerFormat: '<small>日期:{point.key}</small><br>',
            pointFormat: '<span style="color:{point.color}">{series.name}</span> : <b>{point.y}</b><br>'
            // pointFormat: '<b>{point.x} :</b>' + 'Count: <b>{point.y:,.0f}</b>',
        },
        //點擊點點資料顯示
        plotOptions: {
            series: {
                cursor: 'pointer',
                className: 'popup-on-click',
                marker: {
                    lineWidth: 1
                }
            }
        },
        credits: {
            enabled: false
        },
        //右上角功能按鈕
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: ["viewFullscreen", "printChart", "separator", "downloadPNG", "downloadJPEG", "downloadPDF", "downloadSVG", "separator", "downloadCSV", "downloadXLS"],
                }
            }
        },
        //資料
        series: chartData
        // series: [
        //     {
        //         name: '正大門攝影A',
        //         data: [1299, 828, 2345, 134, 462, 234, 533, 808, 846, 426, 656, 869, 908, 597, 457, 654, 855, 890, 456, 533, 808, 846, 426, 656, 134, 462, 234, 533, 462, 234]
        //     },
        //     {
        //         name: '正大門攝影B',
        //         data: [908, 597, 457, 654, 855, 890, 456, 533, 808, 846, 426, 656, 134, 462, 234, 533, 462, 234, 533, 808, 1299, 828, 2345, 134, 462, 234, 533, 808, 846, 426]
        //     },
        //     {
        //         name: '電梯口',
        //         data: [533, 808, 846, 426, 656, 869, 908, 597, 457, 654, 855, 890, 456, 1299, 828, 2345, 134, 462, 234, 533, 808, 846, 426, 656, 134, 462, 234, 533, 462, 234]
        //     },
        //     {
        //         name: '文化資產保存研究中心門口',
        //         data: [908, 597, 457, 654, 855, 890, 456, 533, 1299, 828, 2345, 134, 462, 234, 533, 808, 846, 426, 656, 869, 808, 846, 426, 656, 134, 462, 234, 533, 462, 234]
        //     },
        //     {
        //         name: '展覽室D即時影像',
        //         data: [597, 457, 654, 855, 890, 456, 533, 1299, 828, 2345, 134, 462, 234, 533, 808, 846, 426, 656, 869, 908, 597, 457, 654, 855, 890, 456, 533, 808, 846, 426]
        //     },
        //     {
        //         name: '全部總數',
        //         data: [5676, 6473, 6537, 6653, 6546, 6535, 5333, 1299, 8328, 2435, 1324, 4362, 2324, 5332, 8083, 8346, 4226, 6526, 8269, 9082, 5917, 4517, 6254, 8255, 1890, 1231, 1245, 3256, 2364, 6413]
        //     }
        // ]
    });
}


//=== 男女比例圓餅圖 ===
function pieChart(box, colors, tit, date, pieData) {
    Highcharts.chart(box, {//container1
        chart: {
            type: 'pie'
        },
        colors: colors,//['#88cff9', '#f98888']
        title: {
            text: tit//客群性別分佈佔比
        },
        subtitle: {
            text: date
            // text: '2021-05-10 - 2021/05/14'
        },

        accessibility: {
            announceNewData: {
                enabled: true
            },
            point: {
                valueSuffix: '%'
            }
        },

        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.2f}%'
                    // format: '{point.name}: {point.y:.1f}%'
                }
            }
        },
        credits: {
            enabled: false
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b>'
        },
        //右上角功能按鈕
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: ["viewFullscreen", "printChart", "separator", "downloadPNG", "downloadJPEG", "downloadPDF", "downloadSVG", "separator", "downloadCSV", "downloadXLS"],
                }
            }
        },

        series: [
            {
                name: "",
                colorByPoint: true,
                data: pieData
                // data: [
                //     {
                //         name: "男",
                //         y: 53.11,
                //     },
                //     {
                //         name: "女",
                //         y: 46.89,
                //     },
                // ]
            }
        ]
    });
}

//=== 年齡分佈 ===
// function pieChartTwo(date, pieData) {
//     Highcharts.chart('container2', {
//         chart: {
//             type: 'pie'
//         },
//         colors: ['#FFAD88', '#B978FA', '#F88D8D', '#FFB3B3', '#9ABCEF', '#F3E15D', '#B2C15A'],
//         title: {
//             text: '客群年齡分佈佔比'
//         },
//         subtitle: {
//             text: date
//             // text: '2021-05-10 - 2021/05/14'
//         },

//         accessibility: {
//             announceNewData: {
//                 enabled: true
//             },
//             point: {
//                 valueSuffix: '%'
//             }
//         },

//         plotOptions: {
//             series: {
//                 dataLabels: {
//                     enabled: true,
//                     format: '{point.name}: {point.y:.1f}%'
//                 }
//             }
//         },
//         credits: {
//             enabled: false
//         },

//         tooltip: {
//             headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
//             pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b>'
//         },

//         series: [
//             {
//                 name: "",
//                 colorByPoint: true,
//                 data: pieData
//                 // data: [
//                 //     {
//                 //         name: "(35-44歲)",
//                 //         y: 32.56,
//                 //     },
//                 //     {
//                 //         name: "(45-54歲)",
//                 //         y: 27.57,
//                 //     },
//                 //     {
//                 //         name: "(25-34歲)",
//                 //         y: 12.23,
//                 //     },
//                 //     {
//                 //         name: "(18-24歲)",
//                 //         y: 8.58,
//                 //     },
//                 //     {
//                 //         name: "(55-64歲)",
//                 //         y: 9.02,
//                 //     },
//                 //     {
//                 //         name: "(65+歲)",
//                 //         y: 2.42,
//                 //     },
//                 //     {
//                 //         name: "(0-17歲)",
//                 //         y: 7.62,
//                 //     }
//                 // ]
//             }
//         ]
//     });
// }
