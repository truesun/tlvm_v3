$(function () {
    // var tokena = '677560b911a5bff1eb70588510da201b472d95bce5913593d9a5016439ddd5c0';
    var cam = {
        "FinTimestamp": "2021-06-10T07:32:24.232+0000##0902-882",
        "vcam_uid": "ea8e1f99fc4d47ca8e7fc3936a19e9de",
        "ws_url": "wss://oregon-p1-stage-ss-origin-1-1.beseye.com:443/ea8e1f99fc4d47ca8e7fc3936a19e9de/webrtc-session.json",
        "application_name": "live-origin-record",
        "stream": "{enc_ea8e1f99}benIJa9G2DnMEyH9FLXkenLbHyphBDxSUHv7L_Ey_2lYv_eVAHRM6K85rOK5zO-j",
        "stable_upstreaming": true
    }
    var cam2 = {
        "FinTimestamp": "2021-06-10T08:20:54.272+0000##0902-882",
        "vcam_uid": "67bdb795665d4c93aace2cda365e4ba6",
        "ws_url": "wss://oregon-p1-stage-ss-origin-1-5.beseye.com:443/67bdb795665d4c93aace2cda365e4ba6/webrtc-session.json",
        "application_name": "live-origin-record",
        "stream": "{enc_67bdb795}BiY1hrJ9DzAoHIbsjTXtErNt6QL2rTAwwcPG9KWHeugT4JJlV63mIFxImrRoqzp1",
        "stable_upstreaming": true
    }
    // livePlay('remoteVideo', tokena, cam, '#sdpDataTag');
    // livePlay('remoteVideo2', tokena, cam2, '#sdpDataTag2');
    // pageReady();
    // start();
    // $.ajax({
    //     url: 'https://api.map.baidu.com/location/ip?ak=ia6HfFL660Bvh43exmH9LrI6',
    //     type: 'POST',
    //     dataType: 'jsonp',
    //     success: function (data) {
    //         console.log(data.content);
    //         console.log(data.content.address_detail.province);
    //         // $('#ip').html(data.content.address_detail.province + "," + data.content.address_detail.city);
    //     }
    // });
    // console.log(returnCitySN["cip"]);
    // $('#ip').html(returnCitySN["cip"]);
    // var returnCitySN = { "cip": "36.225.144.38", "cid": "710000", "cname": "台湾省" };
    // $('#ip').html(returnCitySN.cip + ',' + returnCitySN.cid + ',' + returnCitySN.cname);
    // console.log(returnCitySN.cip, returnCitySN.cid, returnCitySN.cname)

    // var uuid = new DeviceUUID().get();
    // console.log('uuid:', uuid)

    //=== 取得API ===
    //== 取的token ==
    var token;
    var apiLink1 = '/overview.aspx?Type=1'
    $.get(apiLink1, function (data) {
        console.log(data);
        var aipData = JSON.parse(data);
        token = aipData[0].access_token;
    });

    //== 取的攝影機列表 ==
    var apiLink2 = '/overview.aspx?Type=2'
    var cameraList = [];
    $.get(apiLink2, function (data) {
        console.log(data);
        console.log(token);
        var aipData = JSON.parse(data);
        $.each(aipData, function (ind, val) {
            var { CameraUID } = val;
            cameraList.push(CameraUID);
        })
        //cameraList.shift();
        // console.log(aipData[0].CameraUID);
        // console.log(returnCitySN["cip"]);
        // var camera1 = aipData[0].CameraUID;
        // var clientIp = returnCitySN["cip"];
        // $.ajax({
        //     url: 'https://oregon-p1-stage-api-1.beseye.com/open_api/v1/client_credential/dvr_playlist_info/' + camera1 + '?access_token=' + token + '&vcam_uid=' + camera1 + '&start_time=1621180800000&duration=12079&client_ip=' + clientIp,
        //     type: 'GET',
        //     // dataType: 'jsonp',
        //     success: function (data) {
        //         console.log(data);
        //     }
        // });
        // https://oregon-p1-stage-api-1.beseye.com/open_api/v1/client_credential/vcam_id_list?access_token=677560b911a5bff1eb70588510da201b472d95bce5913593d9a5016439ddd5c0
    });
    //== 取的 live 資訊 ==
    // a6aacd70dbc84b9c9e434640b38a17d2
    // 台文館咖啡廳 台文館客情 台文館正門 台文館電梯口-1 台文館電梯口-2
    //http://whitenight.work/DVRMPDLive.aspx?Type=3&access_token=677560b911a5bff1eb70588510da201b472d95bce5913593d9a5016439ddd5c0&vcam_uid=ea8e1f99fc4d47ca8e7fc3936a19e9de&client_ip=127.0.0.1
    //== 取得live ==
    console.log('cameraList', cameraList)
    var clientIp = returnCitySN["cip"];
    console.log(clientIp);
    //== 全攝影機 ==
    $.each(cameraList, function (ind, val) {
        var videoInd = ind + 1;
        //= 取得攝影機 cam 資料 =
        var apiLinkLive = '/DVRMPDLive.aspx?Type=3&access_token=' + token + '&vcam_uid=' + val + '&client_ip=' + clientIp;
        var videoBox = 'remoteVideo' + videoInd;
        var videoNote = '#sdpDataTag' + videoInd
        $.get(apiLinkLive, function (data) {
            // console.log(data);
            var apiData = JSON.parse(data);
            console.log('=== live API ===');
            console.log(apiData);
            livePlay(videoBox, token, apiData, videoNote);
            // token = aipData[0].access_token;
        });
    });
    //== 單一 ==
    // var apiLinkLive = '/DVRMPDLive.aspx?Type=3&access_token=' + token + '&vcam_uid=a6aacd70dbc84b9c9e434640b38a17d2&client_ip=' + clientIp;
    // $.get(apiLinkLive, function (data) {
    //     // console.log(data);
    //     var apiData = JSON.parse(data);
    //     console.log('=== live API ===');
    //     console.log(apiData);
    //     livePlay('remoteVideo2', token, apiData, '#sdpDataTag2');
    //     // token = aipData[0].access_token;
    // });

    // var apiLinkReplay = '/DVRMPDLive.aspx?Type=1&access_token=' + token + '&vcam_uid=ea8e1f99fc4d47ca8e7fc3936a19e9de&client_ip=' + clientIp + '&start_time=1622540407540';
    // $.get(apiLinkReplay, function (data) {
    //     // console.log(data);
    //     var apiData = JSON.parse(data);
    //     console.log('=== replay API ===');
    //     console.log(apiData);
    //     // livePlay('remoteVideo2', token, apiData, '#sdpDataTag2');
    //     // token = aipData[0].access_token;
    // });



    // $.ajax({
    //     url: 'https://oregon-p1-stage-api-1.beseye.com/open_api/v1/client_credential/vcam_id_list?access_token=677560b911a5bff1eb70588510da201b472d95bce5913593d9a5016439ddd5c0',
    //     type: 'GET',
    //     dataType: 'jsonp',
    //     // dataType: "jsonp",
    //     // jsonpCallback: "cb",
    //     success: function (data) {
    //         console.log('=== seesee ===');
    //         console.log(data);
    //     }
    // });

    // $.getJSON('https://oregon-p1-stage-api-1.beseye.com/open_api/v1/client_credential/vcam_id_list?access_token=677560b911a5bff1eb70588510da201b472d95bce5913593d9a5016439ddd5c0&jsoncallback=?', function (data) {
    //     console.log('=== seesee ===');
    //     console.log(data);
    // });
});