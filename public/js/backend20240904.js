$(function () {
	// http://whitenight.work/overview.aspx
	// Type = 1(access_token)
	// Type = 2(CameraUID)
	// Type = 3(每日人流數據)
	// Type = 4(客群性別)
	// Type = 5(客群年記)
	// var apiDomain = 'https://whitenight.work';
	var apiDomain = 'https://demotlvm.site';
	$.ajaxSetup({
		async: false,
		crossDomain: true,
		beforeSend: function (xhr, options) {
			options.url = apiDomain + options.url;
		}
	});

	//關閉任何的lightbox
	$("#mask,.lg-bot .btn-cancel").on("click", function () {
		$("#mask,.lg-box").fadeOut(300);
	});

	//打開lightbox
	$("#btnOpenMesg").on("click", function () {
		$("#lgMesg,#mask").fadeIn(300);
	});
	$("#btnOpenBig").on("click", function () {
		$("#lgBig,#mask").fadeIn(300);
	});

	//側邊欄選單
	$('#closeNav').on('click', function () {
		$('#closeNav').animate({ left: '-26px' }, 300);
		$('.side-nav').animate({ width: '0' }, 300);
		$('.btn-side-open').animate({ left: '0' }, 400);
	});

	$('#openNav').on('click', function () {
		$('#closeNav').animate({ left: '180px' }, 300);
		$('.side-nav').animate({ width: '180px' }, 300);
		$('.btn-side-open').animate({ left: '-26px' }, 400);
	});

	//上傳檔案
	// $('#uploadFile').change(function () {
	// 	var filename = $(this).val();
	// 	var lastIndex = filename.lastIndexOf("\\");
	// 	if (lastIndex >= 0) {
	// 		filename = filename.substring(lastIndex + 1);
	// 	}
	// 	$('#fileNameBox').text(filename);
	// });

	$('[uploadfile]').change(function () {
		var filename = $(this).val();
		var lastIndex = filename.lastIndexOf("\\");
		if (lastIndex >= 0) {
			filename = filename.substring(lastIndex + 1);
		}
		$(this).parent().parent().find('span').text(filename);
	});

	//增加標籤
	$('#btnTagAdd').on('click', function () {
		$('#tagCreat').show();
	});
	$('#btnTagAdd2').on('click', function () {
		$('#tagCreat2').show();
	});

	//切換中英輸入匡
	var $transBox = $('#trans1');
	$('[transBox]').find('[enBtn]').on('click', function () {
		var hasAct = $(this).hasClass('act');
		if (hasAct) {
			$(this).removeClass('act');
			$(this).parent().parent().find('[zhTrans]').show();
			$(this).parent().parent().find('[enTrans]').hide();
		} else {
			$(this).addClass('act');
			$(this).parent().parent().find('[zhTrans]').hide();
			$(this).parent().parent().find('[enTrans]').show();
		}
	});

});
// $(window).load(function () {
// 	//側邊欄nav height
// 	sideNav();
// })
$(window).resize(function () {
	sideNav();
});

//上傳圖片的按鈕
function upload_click(obj) {
	var fileEvent = $(obj).parent().find('input[type=file]');
	fileEvent.click();
}

//側邊欄nav height
function sideNav() {
	var _navH = $('.side-nav').height();
	var _logo = $('.side-logo').outerHeight();
	var _tit = $('.backend-tit').outerHeight();
	var _info = $('.user-info').outerHeight();
	var _sideFoot = $('.side-footer').outerHeight();
	//console.log( _navH,_logo,_tit,_info,_sideFoot);
	$('.nav-box').height(_navH - (_logo + _tit + _info + _sideFoot + 50));
}


//=== lg內選過的tag顯示至列表 ===
//= lg input area, tag 頁面顯示匡 , 整體 lg
// pushTag('#authorList','#authorTagArea','#lgAuthorTagBox');
function pushTag(lgList, tagArea, lgTagBox) {
	var $lgList = $(lgList);
	var $tagArea = $(tagArea);
	// var $lgTagBox = $(lgTagBox);
	var tagList = [];
	//= lg內容盒 =
	$lgList.find('input:checked').each(function (ind, item) {
		var tagId = $(item).attr('id');
		var tagName = $(item).parent().find('span').html();
		tagList.push({ 'tagId': tagId, 'tagName': tagName });
	});
	//== 清除顯示匡的tag ==
	$tagArea.html('');
	$.each(tagList, function (ind, val) {
		var tag = '<li>' +
			'<div class="tag-name">' + val.tagName + '</div>' +
			'<div class="tag-del" data-del="' + val.tagId + '">' +
			'<i class="fad fa-times" title="刪除" alt="刪除"></i>' +
			'</div>' +
			'</li>';
		$tagArea.append(tag);
	});
	//=== 是否顯示無資料 ===
	tagNoData(tagArea);
	$(lgTagBox + ',#mask').fadeOut();
	//= 加入tag後綁定點擊功能 刪除功能 =
	$tagArea.find('.tag-del').on('click', function () {
		var delId = $(this).data('del');
		console.log(delId);
		$(this).parent().remove();
		//= 消除lg的勾勾狀態 =
		$lgList.find('#' + delId).prop('checked', false);
		//=== 是否顯示無資料 ===
		tagNoData(tagArea);
	});
}
//=== 是否顯示無資料 ===
function tagNoData(tagArea) {
	var $tagArea = $(tagArea);
	var tagL = $tagArea.find('li').length;
	//console.log(tagL);
	tagL == 0 ? $tagArea.next('.no-data').show() : $tagArea.next('.no-data').hide();
}