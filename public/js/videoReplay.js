// const mediaArray = [
//     {
//         "server": "https://oregon-p1-stage-ss-edge-1-2.beseye.com:1934/playlist",
//         "stream": "{enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHCH-t9ZhdD_GKwJwf1SVphRr9e4eG9birYOPdF-Si7IPKJ2hKX77oJEgvhpmNahZnJiSdDhKHSZBDDWLC1eHUyAlhyQqSMEEnUsXhg7PErMkw==/manifest.mpd",
//         "startTime": "1622540027470",
//         "duration": "10013",
//         "manifestUri": "https://oregon-p1-stage-ss-edge-1-2.beseye.com:1934/playlist?host=oregon-p1-stage-ss-edge-1-2.beseye.com&token=8438c3cc0e4f8bf3c7cbf18ceba36c05e025a9dca9789ebdc08483cd78917f8c&dd={Web}_{KW-External}_{12345678}&vci=ea8e1f99fc4d47ca8e7fc3936a19e9de&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHCH-t9ZhdD_GKwJwf1SVphRr9e4eG9birYOPdF-Si7IPKJ2hKX77oJEgvhpmNahZnJiSdDhKHSZBDDWLC1eHUyAlhyQqSMEEnUsXhg7PErMkw==/manifest.mpd",
//     },
//     {
//         "server": "https://oregon-p1-stage-ss-edge-1-2.beseye.com:1934/playlist",
//         "stream": "{enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHCH-t9ZhdD_GKwJwf1SVphRr9e4eG9birYOPdF-Si7IPMkj5SVoaFqVSnr7IAI8h1GtIESRd-XEy6xSoFauvLCYHcGKqx880bFZYonF_dYXJA==/manifest.mpd",
//         "startTime": "1622540037321",
//         "duration": "10052",
//         "manifestUri": "https://oregon-p1-stage-ss-edge-1-2.beseye.com:1934/playlist?host=oregon-p1-stage-ss-edge-1-2.beseye.com&token=8438c3cc0e4f8bf3c7cbf18ceba36c05e025a9dca9789ebdc08483cd78917f8c&dd={Web}_{KW-External}_{12345678}&vci=ea8e1f99fc4d47ca8e7fc3936a19e9de&videoArray={enc_ea8e1f99}jTK-cUJ1w71_fIdifGSznRjZuVB5zNSb3JTNhZp5_yZ07feprcJDlRFnCdvXU4e4m1yK2sXxMjDXoZFwNUbmFjUn9FNRbyTRFIKzlZvvOHCH-t9ZhdD_GKwJwf1SVphRr9e4eG9birYOPdF-Si7IPMkj5SVoaFqVSnr7IAI8h1GtIESRd-XEy6xSoFauvLCYHcGKqx880bFZYonF_dYXJA==/manifest.mpd",
//     },
// ]

var playingPlayer;
var video1 = document.getElementById('video1');
var video2 = document.getElementById('video2');
var player1 = undefined
var player2 = undefined
var players = {}
var playlist = []
var isPlaying = false
// == 啟動回放 ==
// document.addEventListener('DOMContentLoaded', initApp(mediaArray));

function checkUndefined(x) {
    return typeof x == "undefined"
}

function initApp(data) {
    console.log('=== 1.initApp ===');

    // Install built-in polyfills to patch browser incompatibilities.
    shaka.polyfill.installAll()

    // Check to see if the browser supports the basic APIs Shaka needs.
    if (shaka.Player.isBrowserSupported()) {
        // Everything looks good!
        initPlayer()
    } else {
        // This browser does not have the minimum set of APIs we need.
        console.error('Browser not supported!')
    }
    updatePlaylist(data)
}

function initPlayer() {
    console.log('=== 2.initPlayer ===');
    // Create a Player instance.
    video2.style.display = 'none';
    player1 = new shaka.Player(video1);
    player2 = new shaka.Player(video2);
    player1.video = video1
    player2.video = video2
    player1.shouldPlay = false
    player2.shouldPlay = false
    player1.firstPlay = false
    player2.firstPlay = false
    players = { mainPlayer: player1, preloadPlayer: player2 }
    // window.player1 = player1
    // window.player2 = player2
    setupPlayerEventListeners()
    setupVideoEventListeners()
}

function setupPlayerEventListeners() {
    console.log('=== 3.setupPlayerEventListeners ===');
    // shaka player events: [error]
    player1.addEventListener('error', onPlayerError)
    player2.addEventListener('error', onPlayerError)
}

function setupVideoEventListeners() {
    console.log('=== 4.setupVideoEventListeners ===');
    // video events: [play,canplay,emptied,ended,error...]
    player1.video.addEventListener('play', onVideoPlay.bind(player1))
    player1.video.addEventListener('ended', onVideoEnded.bind(player1))
    player1.video.addEventListener('timeupdate', onVideoTimeUpdate.bind(player1))
    player2.video.addEventListener('play', onVideoPlay.bind(player2))
    player2.video.addEventListener('ended', onVideoEnded.bind(player2))
    player2.video.addEventListener('timeupdate', onVideoTimeUpdate.bind(player2))
}

//  --------------------------------------------
//  Shaka player event
//  --------------------------------------------
function onPlayerError() {
    // Extract the shaka.util.Error object from the event.
    onError(event.detail)
}

function onError(error) {
    console.log('=== 5.onError ===');
    console.error('Error code', error.code, 'object', error,)
    alert('Error code: ' + error.code + '\nError: ' + error,)
}

function onPlayerLoaded() {
    console.log('=== 6.onPlayerLoaded ===');
    var player = this
    console.log('onPlayerLoaded', player.video)
    player.loaded = true
    player.video.play()
}

//  --------------------------------------------
//  Video event
//  --------------------------------------------
function onVideoPlay() {
    console.log('=== 7.onVideoPlay ===');
    var player = this
    console.log('onVideoPlay', player.video)
    player.firstPlay = true
}

function onVideoEnded() {
    console.log('=== 8.onVideoEnded ===');
    console.log('onVideoEnded', this.video)
    // switch player
    var mainPlayer = players.preloadPlayer
    var preloadPlayer = players.mainPlayer
    preloadPlayer.media = undefined
    preloadPlayer.ready = false
    players.preloadPlayer = preloadPlayer
    players.mainPlayer = mainPlayer
    if (mainPlayer.ready) {
        mainPlayer.video.play()
    } else {
        preloadPlayer.video.style.display = 'none'
    }
    // preload player load new streaming
    setTimeout(preparePreloadStreaming, 1000)
}

function onVideoTimeUpdate() {
    console.log('=== 9.onVideoTimeUpdate ===');
    player = this
    if (!player.loaded) {
        return
    }
    if (player == players.preloadPlayer) {
        console.log('=== 9.1 ===');
        console.log('onVideoTimeUpdate preload player', player.video)
        player.video.pause()
        player.ready = true
    } else {
        if (player.firstPlay) {
            player.firstPlay = false
            console.log('=== 9.2 ===');
            console.log('onVideoTimeUpdate main player firstPlay', player.video)
            showHideVideo()
            // [custom] onPlay callback should be called to change UI
        }
        if (isPlaying) {
            // [custom] timer callback should be called to update time
        }
    }
}

//  --------------------------------------------
//   main action
//  --------------------------------------------
function updatePlaylist(newPlaylist) {
    console.log('=== 10.updatePlaylist ===');
    var mainPlayer = players.mainPlayer
    var preloadPlayer = players.preloadPlayer
    if (newPlaylist.length == 0) {
        console.error('Got empty playlist')
        return
    }
    if (isPlaying) {
        if (playlist == 0 && checkUndefined(preloadPlayer.media)) {
            preloadPlayer.media = newPlaylist[0]
            preloadStreaming(preloadPlayer, newPlaylist[0].manifestUri)
            newPlaylist.shift()
        }
        playlist = playlist.concat(newPlaylist)
    } else {
        // before play
        isPlaying = true
        mainPlayer.ready = false
        preloadPlayer.ready = false
        for (let index = 0; index < newPlaylist.length; index++) {
            const media = newPlaylist[index];
            switch (index) {
                case 0:
                    mainPlayer.media = media
                    loadStreaming(mainPlayer, media.manifestUri)
                    break;
                case 1:
                    preloadPlayer.media = media
                    setTimeout(() => {
                        preloadStreaming(preloadPlayer, media.manifestUri)
                    }, 1000);
                    break;
                default:
                    playlist.push(media)
                    break;
            }

        }
        // [custom] count player end time
    }
}

function preparePreloadStreaming() {
    console.log('=== 11.preparePreloadStreaming ===');
    if (playlist.length == 0) {
        // to-da request new playlist
        console.warn('Out of playlist')
    } else {
        preloadPlayer = players.preloadPlayer
        preloadPlayer.media = playlist[0]
        preloadStreaming(preloadPlayer, playlist[0].manifestUri)
        playlist.shift()
    }
}

function preloadStreaming(player, assetUri, startTime = null, mimeType = 'application/dash+xml') {
    console.log('=== 12.preloadStreaming ===');
    player.shouldPlay = false
    loadStreaming(player, assetUri, startTime, mimeType)
}

function loadStreaming(player, assetUri, startTime = null, mimeType = 'application/dash+xml') {
    console.log('=== 13.preloadStreaming ===');
    console.log('loadStreaming', '\nplayer', player.video, '\nmedia:', player.media, '\nUri:', assetUri, '\nplaylist:', playlist)
    if (assetUri !== '') {
        player.loaded = false
        player.load(assetUri, startTime, mimeType).then(onPlayerLoaded.bind(player)).catch(onError)
    } else {
        // no clip
        var lastDuration = getPlaylistDuration()
        console.log('no clips happened', players, playlist)
        // [custom] callback to play live after lastDuration
    }

}

//  --------------------------------------------
//   User action
//  --------------------------------------------

function captureSnapshot() {
    var video = players.mainPlayer.video
    canvas = document.createElement('canvas')
    canvas.width = 1920
    canvas.height = 1080
    ctx = canvas.getContext('2d')
    ctx.drawImage(video, 0, 0, canvas.width, canvas.height)
    dataURI = canvas.toDataURL('image/jpeg')
    link = document.createElement('a')
    link.download = 'snapshot.png'
    link.href = dataURI
    link.click()
}

//  --------------------------------------------
//   UI control
//  --------------------------------------------

function showHideVideo() {
    console.log('=== 14.showHideVideo ===');
    players.mainPlayer.video.style.display = 'block'
    players.preloadPlayer.video.style.display = 'none'
}


//  --------------------------------------------
//   helper function
//  --------------------------------------------
function getPlaylistDuration() {
    console.log('=== 15.getPlaylistDuration ===');
    var lastDuration = 0
    for (let index = 0; index < playlist.length; index++) {
        const media = playlist[index];
        lastDuration += media.duration
    }
    if (!checkUndefined(players.preloadPlayer.media)) {
        lastDuration += players.preloadPlayer.media.duration
    }
    if (!checkUndefined(players.mainPlayer.media)) {
        lastDuration += players.mainPlayer.media.duration - players.mainPlayer.video.currentTime * 1000
    }
    return lastDuration
}

    // document.addEventListener('DOMContentLoaded', initApp);
// }
